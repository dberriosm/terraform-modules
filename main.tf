resource "gitlab_group_variable" "groupvar" {
    count               = length(var.parameters)
    group               = var.group
    key                 = lookup(var.parameters[count.index], "key")
    value               = lookup(var.parameters[count.index], "value")
    variable_type       = lookup(var.parameters[count.index], "variable_type")
    protected           = lookup(var.parameters[count.index], "protected")
    masked              = lookup(var.parameters[count.index], "masked")
}
