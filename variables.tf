variable "gitlab_url"{
  default = ""
}
variable "gitlab_token"{
  default = ""
}
variable "group" {
  description = "Group ID"
  default     = ""
}

variable "parameters" {
  description = "Parameters list"
  default = [
    {
      key               = ""
      value             = ""
      variable_type     = ""
      protected         = false
      masked            = false
    },
  ]
}
