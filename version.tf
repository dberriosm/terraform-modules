terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.1.0"
    }
  }
  required_version = ">= 0.13"
}

provider "gitlab" {
  base_url  = var.gitlab_url
  token     = var.gitlab_token
}
